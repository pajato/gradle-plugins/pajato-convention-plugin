@file:Suppress("UnstableApiUsage")

rootProject.name = "pajato-convention-plugin"

dependencyResolutionManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        mavenLocal()
    }
}
