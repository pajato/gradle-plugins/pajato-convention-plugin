import com.pajato.plugins.PajatoPublish

plugins {
    id("org.jetbrains.kotlin.jvm")
}

kotlin {
    jvmToolchain { languageVersion.set(JavaLanguageVersion.of(11)) }
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks {
    compileKotlin { kotlinOptions.jvmTarget = "11" }
    compileTestKotlin { kotlinOptions.jvmTarget = "11" }
    test {
        useJUnitPlatform()
        testLogging.showStandardStreams = true
    }
}
