package com.pajato.plugins

import org.gradle.api.Project
import org.gradle.api.publish.maven.MavenPom
import org.gradle.api.publish.maven.MavenPomDeveloper
import org.gradle.api.publish.maven.MavenPomDeveloperSpec
import org.gradle.api.publish.maven.MavenPomOrganization

object PajatoPublish {
    private const val POM_ORGANIZATION_URL = "https://pajato.com/"
    private const val POM_ORGANIZATION_NAME = "Pajato Technologies LLC"
    private const val POM_LICENSE_NAME = "The GNU General Public License, Version 3"
    private const val POM_LICENSE_URL = "https://www.gnu.org/copyleft/lesser.html"
    private const val POM_LICENSE_DIST = "repo"

    fun populatePom(pom: MavenPom, project: Project) {
        val groupName = "${project.group}".split(".").last()
        fun populatePomBasic() = pom.apply {
            name.set(project.name)
            description.set(project.description)
            url.set("https://gitlab.com/pajato/$groupName/${project.name}")
        }

        fun populatePomLicenses() = pom.apply {
            licenses {
                license {
                    name.set(POM_LICENSE_NAME)
                    url.set(POM_LICENSE_URL)
                    distribution.set(POM_LICENSE_DIST)
                }
            }
        }

        fun populatePomScm() = pom.apply {
            scm {
                url.set("https://gitlab.com/pajato/$groupName/${project.name}")
                connection.set("scm:git:https://gitlab.com/pajato/$groupName/${project.name}.git")
                developerConnection.set("scm:git:git@gitlab.com:pajato/$groupName/${project.name}.git")
            }
        }

        fun populatePomDevelopers() = pom.apply {
            fun populatePomDevelopers(spec: MavenPomDeveloperSpec) = pom.apply {
                fun populatePomDeveloper(dev: MavenPomDeveloper) = pom.apply {
                    fun populatePomOrganization(org: MavenPomOrganization) {
                        org.name.set(POM_ORGANIZATION_NAME)
                        org.url.set(POM_ORGANIZATION_URL)
                    }

                    dev.id.set("${project.property("SONATYPE_NEXUS_USERNAME")}")
                    dev.name.set("${project.property("SONATYPE_NEXUS_USERNAME")}")
                    organization { populatePomOrganization(this) }
                }

                spec.developer { populatePomDeveloper(this) }
            }

            developers { populatePomDevelopers(this) }
        }

        populatePomBasic()
        populatePomLicenses()
        populatePomScm()
        populatePomDevelopers()
    }
}
