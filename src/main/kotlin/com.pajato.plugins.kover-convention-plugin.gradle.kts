plugins {
    id("org.jetbrains.kotlinx.kover")
}

koverReport {
    defaults {
        html {
            onCheck = true
            setReportDir(layout.buildDirectory.dir("coverage-report"))
        }

        verify {
            onCheck = true
            rule {
                isEnabled = true
                entity = kotlinx.kover.gradle.plugin.dsl.GroupingEntityType.APPLICATION

                bound {
                    minValue = 100
                    metric = kotlinx.kover.gradle.plugin.dsl.MetricType.INSTRUCTION
                    aggregation = kotlinx.kover.gradle.plugin.dsl.AggregationType.COVERED_PERCENTAGE
                }
            }
        }
    }
}
