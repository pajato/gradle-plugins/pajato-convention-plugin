import com.pajato.plugins.PajatoPublish

plugins {
    id("org.jetbrains.kotlin.jvm")
    id("maven-publish")
    signing
}

val sourcesJar by tasks.creating(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.getByName("main").allSource)
}

val testJar by tasks.registering(Jar::class) {
    archiveClassifier.set("tests")
    from(sourceSets.test.get().output)
    exclude("**/*UnitTest*.class")
    exclude("**/*IntegrationTest.class")
    exclude(".lsp")
    exclude("sample_projects")
}

val javadocJar by tasks.creating(Jar::class) {
    archiveClassifier.set("javadoc")
    from("${layout.buildDirectory}/dokka/javadoc")
}
tasks.getByName("javadocJar").dependsOn("dokkaJavadoc")
tasks.getByName("build").dependsOn("dokkaJavadoc")

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = rootProject.name
            from(components["kotlin"])
            suppressAllPomMetadataWarnings()
            artifact(javadocJar)
            artifact(sourcesJar)
            artifact(testJar)
            PajatoPublish.populatePom(pom, project)
        }
    }
}
tasks.getByName("build").dependsOn("publishToMavenLocal")
