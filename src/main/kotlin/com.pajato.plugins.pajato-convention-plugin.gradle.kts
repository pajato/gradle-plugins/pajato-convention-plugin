import org.gradle.kotlin.dsl.invoke

plugins {
    id("com.pajato.plugins.kotlin-convention-plugin")
    id("com.pajato.plugins.ktlint-convention-plugin")
    id("com.pajato.plugins.kover-convention-plugin")
    id("com.pajato.plugins.nexus-convention-plugin")
    id("com.pajato.plugins.publish-convention-plugin")
    id("kotlinx-serialization")
    id("org.jetbrains.dokka")
}

tasks.processTestResources {
    val key = project.property("argus_tmdb_api_key") as String
    include("test.properties")
    filter { line -> line.replace("@argus_tmdb_api_key@", key) }
    include("*")
    include("*/*")
}
